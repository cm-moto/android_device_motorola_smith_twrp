# TWRP Device configuration for Motorola Razr 5G

## Device specifications

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | Octa-core (1x2.4 GHz Kryo 475 Prime & 1x2.2 GHz Kryo 475 Gold & 6x1.8 GHz Kryo 475 Silver)
CHIPSET | Qualcomm SM7250 Snapdragon 765G
GPU     | Adreno 620
Memory  | 8GB
Shipped Android Version | 10
Storage | 256GB
Battery | 2800 mAh
Dimensions | 169.2 x 72.6 x 7.9 mm
Display | 876 x 2142 pixels, 6.2" Foldable P-OLED
Rear Camera  | 48 MP, f/1.7, 26mm (wide), 1/2.0", 0.8µm, PDAF, OIS
Front Camera | 20 MP, f/2.2, (wide), 0.8µm

![Device Picture](https://fdn2.gsmarena.com/vv/pics/motorola/motorola-razr-5g-brush-gold-1.jpg)

### Kernel Source

See /prebuilt/README.md

### How to compile

```sh
. build/envsetup.sh
export LC_ALL=C
lunch omni_smith-eng
make -j4 recoveryimage
```

### Build with TWRP installer

To automatically make the twrp installer, you need to import this commit in the build/make path:
```sh
https://gerrit.omnirom.org/#/c/android_build/+/33182/
```

Then add @osm0sis' standard twrp_abtemplate repo to a local manifest as indicated below (followed by another `repo sync` to download the repo):
```xml
<project name="osm0sis/twrp_abtemplate" path="bootable/recovery/installer" remote="github" revision="master"/>
```

### Copyright
 ```
  /*
  *  Copyright (C) 2013-21 The OmniROM Project
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *
  */
  ```
